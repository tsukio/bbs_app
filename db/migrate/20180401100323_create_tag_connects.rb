class CreateTagConnects < ActiveRecord::Migration[5.1]
  def change
    create_table :tag_connects do |t|
      t.integer :tag_name_id
      t.integer :topic_id

      t.timestamps
    end
  end
end
