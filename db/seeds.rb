# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

topic_title = ["タイガーマスクの出会い",
               "なんで小便って死ぬほど湯切りしたのにパンツにしまった瞬間にi'll be backしてくんの？",
               "どうして人はパン屋さんでトングを持つと、ｶﾁｶﾁと鳴らして威嚇するの？",
               "大相撲を性の対象として見てる奴ちょっとこい",
               "ベイブレードが強いのに彼女ができない",
               "一輪車のタイヤ盗まれた",
               "ふと江頭が暑くなった瞬間",
               "脳なんとかってゲームで記憶力がよくなった",
               "マックでテイクオフしてきた"]
8.times { |i| Topic.create!(title: topic_title[i]) }
TagName.create!(name: "2ch")
8.times { |i| TagConnect.create!(tag_name_id: 1, topic_id: i)}
p_text = ["あれは誰だ?",
          "こん^^",
          "サンドスターで生まれたフレンズ!?",
          "(何のフレンズか)わからないのだ!",
          "たぁーーっ！(困惑)",
          "わからん",
          "ときは",
          "図書館",
          "行くかーっ！",]
9.times { |i| Post.create!(name: "名無しのフレンズ", body: p_text[i], topic_id: 1)}