Rails.application.routes.draw do
  root 'topics#index'
  get 'topics/error', to: 'topics#error', as: :error
  post 'topics/create', to: 'topics#create'
  post 'topics/destroy/:id', to: 'topics#destroy', as: :topic_destroy

  get '/show/:id', to: 'post#show', as: :post_show
  post '/posts/create', to: 'post#create', as: :post_create
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
