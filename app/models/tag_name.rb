class TagName < ApplicationRecord
    has_many :topic, through: :tag_connect
    has_many :tag_connect, dependent: :destroy
    validates :name, uniqueness: true
end
