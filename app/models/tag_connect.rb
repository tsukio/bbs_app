class TagConnect < ApplicationRecord
    belongs_to :tag_name
    belongs_to :topic
    validates :tag_name_id, presence: true
    validates :topic_id, presence: true
end
