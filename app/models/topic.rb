class Topic < ApplicationRecord
    has_many :post, dependent: :destroy
    has_many :tag_name, through: :tag_connect
    has_many :tag_connect, dependent: :destroy
    validates :title, presence: true
end
