class PostController < ApplicationController

    def show
        @topic = Topic.find_by(id: params[:id])
        @post = Post.new(topic_id: params[:id])
        @posts = Post.where(topic_id: params[:id])
        tag = TagConnect.find_by(topic_id: params[:id])
        if tag
            @tags = TagName.where(id: tag.tag_name_id)
        end
    end

    def create
        if params[:post][:name].empty?
            params[:post][:name] = "名無しのフレンズ"
        end
        @post = Post.new(params[:post].permit(:topic_id, :name, :body))
        if @post.save
            redirect_to post_show_path(params[:post][:topic_id])
        else 
            id = params[:post][:topic_id].to_i 
            @topic = Topic.find(id) 
            @posts = Post.where(topic_id: id) 
            render :show 
        end
    end
end