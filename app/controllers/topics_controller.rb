class TopicsController < ApplicationController
  def index
    @topic = Topic.new
    @t_name = TagName.new
    case
    when params[:search] 
      @topics = Topic.where('title LIKE(?)', "%#{params[:search]}%")
    when params[:t_search]
      @tag_name = TagName.find_by(name: params[:t_search])
      if @tag_name
        results = TagConnect.where(tag_name_id: @tag_name.id)
        n = []
        results.each { |result| n.push( result.topic_id ) }
        @topics = Topic.where("id IN (?)", n)
      else
        @topics = nil
      end
    else
      @topics = Topic.all
    end
  end

  def create 
    @topic = Topic.new(title: params[:title])
    if @topic.save
      if params[:t_name].present?
        @t_name = TagName.find_by(name: params[:t_name])
        if @t_name.nil?
          @t_name = TagName.new(name: params[:t_name])
          @t_name.save
        end
        @t_connect = TagConnect.new(topic_id: @topic.id, tag_name_id: @t_name.id)
        if @t_connect.save
          redirect_to root_path
          return
        else
          render error_path
          return
        end
      end
      redirect_to root_path
    else
      render error_path
    end
  end

  def destroy
    @topic = Topic.find(params[:id])
    @tag = TagConnect.where(tag_name_id: params[:id])
    @post = Post.where(topic_id: params[:id])
    if @topic.destroy
      redirect_to root_path
    else
      render root_path
    end
  end

  def error
  end
end
